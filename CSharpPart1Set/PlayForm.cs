﻿using System;
using System.Windows.Forms;

namespace CSharpPart1Set
{
    public partial class PlayForm : Form
    {
        string inputStr;
        int inputNum;
        Set set;

        public PlayForm(Set set)
        {
            InitializeComponent();
            this.set = set; 
        }

        private void inputTextBox_TextChanged(object sender, EventArgs e)
        {
            inputStr = inputTextBox.Text;
            var strs = inputStr.Split(' ', ';', ',');
            int.TryParse(strs[0], out inputNum);
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (inputNum < 1)
                return;
            try
            {
                set.Add(inputNum);
                MessageBox.Show("Элемент " + inputNum + " добавлен");
            }
            catch (OutOfBoundsException ex)
            {
                MessageBox.Show("Попытка добавить значение, большее максимально возможного.");
            }
        }

        private void delButton_Click(object sender, EventArgs e)
        {
            if (inputNum < 1)
                return;
            if (set.FindItem(inputNum))
            {
                set.Remove(inputNum);
                MessageBox.Show("Элемент " + inputNum + " удален");
            }
            else
                MessageBox.Show("Такого элемента не было в множестве.");
        }

        private void checkButton_Click(object sender, EventArgs e)
        {
            if (inputNum < 1)
                return;
            var result = set.FindItem(inputNum);
            if (result)
                MessageBox.Show("Элемент " + inputNum + " найден.");
            else
                MessageBox.Show("Элемент " + inputNum + " не найден.");
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            set.Print("Полученное множество:");
            Close();
        }
    }
}
