﻿using System;
using System.Windows.Forms;

namespace CSharpPart1Set
{
    public partial class SpecialTestForm : Form
    {
        string input1;
        string input2;

        public SpecialTestForm()
        {
            InitializeComponent();
        }

        private void set1_TextChanged(object sender, EventArgs e)
        {
            input1 = set1TB.Text;
        }

        private void set2TB_TextChanged(object sender, EventArgs e)
        {
            input2 = set2TB.Text;
        }

        private void unionButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(input1) || string.IsNullOrEmpty(input2))
            {
                MessageBox.Show("Оба поля должны быть заполнены!");
                return;
            }
            var set1 = new SimpleSet(StartForm.maxElement);
            var set2 = new SimpleSet(StartForm.maxElement);
            var set3 = new BitSet(StartForm.maxElement);
            var set4 = new BitSet(StartForm.maxElement);
            SetsInit(set1, set2, set3, set4);

            var res1 = set1 + set2;
            res1.Print("SimpleSet:");
            var res2 = set3 + set4;
            res2.Print("BitSet:");
        }

        private void intersectButton_Click(object sender, EventArgs e)
        {
            var set1 = new SimpleSet(StartForm.maxElement);
            var set2 = new SimpleSet(StartForm.maxElement);
            var set3 = new BitSet(StartForm.maxElement);
            var set4 = new BitSet(StartForm.maxElement);
            SetsInit(set1, set2, set3, set4);

            var res1 = set1 * set2;
            res1.Print("SimpleSet:");
            var res2 = set3 * set4;
            res2.Print("BitSet:");
        }

        void SetsInit(SimpleSet set1, SimpleSet set2, BitSet set3, BitSet set4)
        {
            set1.Fill(input1);
            set2.Fill(input2);
            set3.Fill(input1);
            set4.Fill(input2);
        }
    }
}
