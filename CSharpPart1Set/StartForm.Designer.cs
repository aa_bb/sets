﻿namespace CSharpPart1Set
{
    partial class StartForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.viewVariantLabel = new System.Windows.Forms.Label();
            this.inputVariantCheckBox = new System.Windows.Forms.CheckBox();
            this.inputTextBox = new System.Windows.Forms.TextBox();
            this.variantNameLabel = new System.Windows.Forms.Label();
            this.startPlayFormButton = new System.Windows.Forms.Button();
            this.maxElementLabel = new System.Windows.Forms.Label();
            this.maxElementTextBox = new System.Windows.Forms.TextBox();
            this.specialTestButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Логическое",
            "Битовое",
            "Мультимножество"});
            this.comboBox1.Location = new System.Drawing.Point(57, 122);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(192, 28);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // viewVariantLabel
            // 
            this.viewVariantLabel.AutoSize = true;
            this.viewVariantLabel.Location = new System.Drawing.Point(53, 87);
            this.viewVariantLabel.Name = "viewVariantLabel";
            this.viewVariantLabel.Size = new System.Drawing.Size(289, 20);
            this.viewVariantLabel.TabIndex = 1;
            this.viewVariantLabel.Text = "Вариант представления множества:";
            // 
            // inputVariantCheckBox
            // 
            this.inputVariantCheckBox.AutoSize = true;
            this.inputVariantCheckBox.Location = new System.Drawing.Point(57, 172);
            this.inputVariantCheckBox.Name = "inputVariantCheckBox";
            this.inputVariantCheckBox.Size = new System.Drawing.Size(338, 24);
            this.inputVariantCheckBox.TabIndex = 3;
            this.inputVariantCheckBox.Text = "Вводить начальное множ-во как строку";
            this.inputVariantCheckBox.UseVisualStyleBackColor = true;
            this.inputVariantCheckBox.CheckedChanged += new System.EventHandler(this.inputVariantCheckBox_CheckedChanged);
            // 
            // inputTextBox
            // 
            this.inputTextBox.Location = new System.Drawing.Point(57, 244);
            this.inputTextBox.Name = "inputTextBox";
            this.inputTextBox.Size = new System.Drawing.Size(335, 26);
            this.inputTextBox.TabIndex = 4;
            this.inputTextBox.TextChanged += new System.EventHandler(this.inputTextBox_TextChanged);
            // 
            // variantNameLabel
            // 
            this.variantNameLabel.AutoSize = true;
            this.variantNameLabel.Location = new System.Drawing.Point(53, 211);
            this.variantNameLabel.Name = "variantNameLabel";
            this.variantNameLabel.Size = new System.Drawing.Size(14, 20);
            this.variantNameLabel.TabIndex = 5;
            this.variantNameLabel.Text = "|";
            // 
            // startPlayFormButton
            // 
            this.startPlayFormButton.Location = new System.Drawing.Point(123, 295);
            this.startPlayFormButton.Name = "startPlayFormButton";
            this.startPlayFormButton.Size = new System.Drawing.Size(196, 39);
            this.startPlayFormButton.TabIndex = 6;
            this.startPlayFormButton.Text = "Начать Играться";
            this.startPlayFormButton.UseVisualStyleBackColor = true;
            this.startPlayFormButton.Click += new System.EventHandler(this.startPlayFormButton_Click);
            // 
            // maxElementLabel
            // 
            this.maxElementLabel.AutoSize = true;
            this.maxElementLabel.Location = new System.Drawing.Point(53, 9);
            this.maxElementLabel.Name = "maxElementLabel";
            this.maxElementLabel.Size = new System.Drawing.Size(274, 20);
            this.maxElementLabel.TabIndex = 7;
            this.maxElementLabel.Text = "Наибольшее возможное значение:";
            // 
            // maxElementTextBox
            // 
            this.maxElementTextBox.Location = new System.Drawing.Point(57, 44);
            this.maxElementTextBox.Name = "maxElementTextBox";
            this.maxElementTextBox.Size = new System.Drawing.Size(192, 26);
            this.maxElementTextBox.TabIndex = 8;
            this.maxElementTextBox.Text = "200";
            this.maxElementTextBox.TextChanged += new System.EventHandler(this.maxElementTextBox_TextChanged);
            // 
            // specialTestButton
            // 
            this.specialTestButton.Location = new System.Drawing.Point(337, 299);
            this.specialTestButton.Name = "specialTestButton";
            this.specialTestButton.Size = new System.Drawing.Size(57, 34);
            this.specialTestButton.TabIndex = 9;
            this.specialTestButton.Text = "+/*";
            this.specialTestButton.UseVisualStyleBackColor = true;
            this.specialTestButton.Click += new System.EventHandler(this.specialTestButton_Click);
            // 
            // StartForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 355);
            this.Controls.Add(this.specialTestButton);
            this.Controls.Add(this.maxElementTextBox);
            this.Controls.Add(this.maxElementLabel);
            this.Controls.Add(this.startPlayFormButton);
            this.Controls.Add(this.variantNameLabel);
            this.Controls.Add(this.inputTextBox);
            this.Controls.Add(this.inputVariantCheckBox);
            this.Controls.Add(this.viewVariantLabel);
            this.Controls.Add(this.comboBox1);
            this.Name = "StartForm";
            this.Text = "Dialog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label viewVariantLabel;
        private System.Windows.Forms.CheckBox inputVariantCheckBox;
        private System.Windows.Forms.TextBox inputTextBox;
        private System.Windows.Forms.Label variantNameLabel;
        private System.Windows.Forms.Button startPlayFormButton;
        private System.Windows.Forms.Label maxElementLabel;
        private System.Windows.Forms.TextBox maxElementTextBox;
        private System.Windows.Forms.Button specialTestButton;
    }
}

