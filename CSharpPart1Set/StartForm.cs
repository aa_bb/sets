﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace CSharpPart1Set
{
    enum SetVariants
    {
        SimpleSet,
        BitSet,
        MultiSet
    };

    public partial class StartForm : Form
    {
        public static int maxElement = 200;
        string maxElementStr = "200";
        string input;
        Set set;
        SetVariants selectedSetVariant;

        public StartForm()
        {
            InitializeComponent();
            inputVariantCheckBox.CheckState = CheckState.Checked;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedSetVariant = (SetVariants)comboBox1.SelectedIndex;
        }

        private void inputTextBox_TextChanged(object sender, EventArgs e)
        {
            input = inputTextBox.Text;
        }

        private void inputVariantCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (inputVariantCheckBox.Checked)
                variantNameLabel.Text = "Введите мн-во как строку: ";
            else
                variantNameLabel.Text = "Введите путь к файлу, содержащему мн-во: ";
        }

        private void maxElementTextBox_TextChanged(object sender, EventArgs e)
        {
            maxElementStr = maxElementTextBox.Text;
        }

        private void startPlayFormButton_Click(object sender, EventArgs e)
        {
            if (!int.TryParse(maxElementStr, out maxElement))
            {
                MessageBox.Show("Число элементов должно иметь числовое значение");
                return;
            }
            if (maxElement <= 0)
            {
                MessageBox.Show("Указан некорректный размер множества. Повторите попытку.");
                return;
            }

            CreateSet();
            if (inputVariantCheckBox.Checked)
                set.Fill(input);
            else
                FillSetFromFile();

            MessageBox.Show(selectedSetVariant.ToString() + " с максимальным числом " + maxElement + " был создан.");
            var f = new PlayForm(set);
            f.Show();
        }

        void FillSetFromFile()
        {
            var list = new List<int>();
            try
            {
                using (StreamReader sr = new StreamReader(input))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                        list.Add(int.Parse(line));
                    set.Fill(list.ToArray());
                }
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show("Пожалуйста, проверьте путь к файлу");
                return;
            }
            catch (ArgumentNullException ex)
            { }
        }

        void CreateSet()
        {
            if (string.IsNullOrEmpty(input))
                MessageBox.Show("Будет создано пустое множество.");

            if (selectedSetVariant == SetVariants.SimpleSet)
                set = new SimpleSet(maxElement);
            else if (selectedSetVariant == SetVariants.BitSet)
                set = new BitSet(maxElement);
            else set = new MultiSet(maxElement);
        }

        private void specialTestButton_Click(object sender, EventArgs e)
        {
            var f = new SpecialTestForm();
            f.Show();
        }
    }
}
