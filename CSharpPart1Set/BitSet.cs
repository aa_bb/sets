﻿using System;

namespace CSharpPart1Set
{
    class BitSet : Set
    {
        int[] m;

        public BitSet(int count)
        {
            MaxElement = count;
            var size = count / 32;
            if (count % 32 != 0)
                size++;
            m = new int[size];
        }

        int GetShifted(int item)
        {
            int getShifted = 1;
            return getShifted << 30 - (item % 32);
        }

        public override void Add(int item)
        {
            if (item > MaxElement)
                throw new OutOfBoundsException("Попытка добавить в множество элемент, больший максимального");
            int getShifted = GetShifted(item);
            m[item / 32] |= getShifted;            
        }

        public override void Remove(int item)
        {
            m[item / 32] ^= GetShifted(item);
        }

        public override bool FindItem(int item)
        {
            int getShifted = GetShifted(item);
            return (m[item / 32] & getShifted) != 0;
        }

        public static BitSet operator *(BitSet set1, BitSet set2)
        {
            var maxElem = Math.Max(set1.MaxElement, set2.MaxElement);
            var set3 = new BitSet(maxElem);
            for (int i = 0; i < maxElem; i++)
                if (set1.FindItem(i) && set2.FindItem(i))
                    set3.Add(i);
            return set3;
        }

        public static BitSet operator +(BitSet set1, BitSet set2)
        {
            var maxElem = Math.Max(set1.MaxElement, set2.MaxElement);
            var set3 = new BitSet(maxElem);
            for (int i = 0; i < maxElem; i++)
                if (set1.FindItem(i) || set2.FindItem(i))
                    set3.Add(i);
            
            return set3;
        }
    }
}
