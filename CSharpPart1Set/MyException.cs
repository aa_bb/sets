﻿using System;

namespace CSharpPart1Set
{
    class OutOfBoundsException : Exception
    {
        public OutOfBoundsException(string message) : base(message) {}
    }
}
