﻿using System;

namespace CSharpPart1Set
{
    class SimpleSet : Set
    {
        bool[] m;

        public SimpleSet(int count)
        {
            MaxElement = count;
            m = new bool[MaxElement];
        }

        public override void Add(int item)
        {
            if (item > MaxElement)
                throw new OutOfBoundsException("Попытка добавить в множество элемент, больший максимального");
            m[item] = true;
        }

        public override void Remove(int item)
        {
            m[item] = false;
        }

        public override bool FindItem(int item)
        {
            return m[item];
        }

        public static SimpleSet operator *(SimpleSet set1, SimpleSet set2)
        {
            var maxElem = Math.Max(set1.MaxElement, set2.MaxElement);
            var set3 = new SimpleSet(maxElem);
            for (int i = 0; i < maxElem; i++)
                if (set1.FindItem(i) && set2.FindItem(i))
                    set3.Add(i);
            return set3;
        }

        public static SimpleSet operator +(SimpleSet set1, SimpleSet set2)
        {
            var maxElem = Math.Max(set1.MaxElement, set2.MaxElement);
            var set3 = new SimpleSet(maxElem);
            for (int i = 0; i < maxElem; i++)
            {
                if (set1.FindItem(i) || set2.FindItem(i))
                    set3.Add(i);
            }
            return set3;
        }
    }
}
