﻿using System.Windows.Forms;

namespace CSharpPart1Set
{
    public abstract class Set
    {
        protected int MaxElement { get; set; }
        public abstract void Add(int item);
        public abstract void Remove(int item);
        public abstract bool FindItem(int item);

        public void Fill(string input)
        {
            if (string.IsNullOrEmpty(input))
                return;
            int tmp;
            var items = input.Split(' ', ',', ';');
            foreach (var item in items)
                if (int.TryParse(item, out tmp))
                    Add(tmp);
                else
                {
                    MessageBox.Show("Попытка добавить нечисловое значение.");
                    return;
                }
        }

        public void Fill(int[] input)
        {
            foreach (var item in input)
                Add(item);
        }

        public void Print(string message)
        { 
            var result = "";
            for (int i = 0; i < MaxElement; i++)
            {
                if (FindItem(i))
                    result += i + " ";
            }
            MessageBox.Show(message + "\n" + result);
        }
    }
}
