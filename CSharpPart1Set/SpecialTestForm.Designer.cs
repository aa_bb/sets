﻿namespace CSharpPart1Set
{
    partial class SpecialTestForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.set1TB = new System.Windows.Forms.TextBox();
            this.set2TB = new System.Windows.Forms.TextBox();
            this.unionButton = new System.Windows.Forms.Button();
            this.intersectButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // set1TB
            // 
            this.set1TB.Location = new System.Drawing.Point(54, 28);
            this.set1TB.Name = "set1TB";
            this.set1TB.Size = new System.Drawing.Size(297, 26);
            this.set1TB.TabIndex = 0;
            this.set1TB.Text = "Введите множество 1";
            this.set1TB.TextChanged += new System.EventHandler(this.set1_TextChanged);
            // 
            // set2TB
            // 
            this.set2TB.Location = new System.Drawing.Point(51, 85);
            this.set2TB.Name = "set2TB";
            this.set2TB.Size = new System.Drawing.Size(297, 26);
            this.set2TB.TabIndex = 1;
            this.set2TB.Text = "Введите множество 2";
            this.set2TB.TextChanged += new System.EventHandler(this.set2TB_TextChanged);
            // 
            // unionButton
            // 
            this.unionButton.Location = new System.Drawing.Point(102, 142);
            this.unionButton.Name = "unionButton";
            this.unionButton.Size = new System.Drawing.Size(79, 36);
            this.unionButton.TabIndex = 2;
            this.unionButton.Text = "+";
            this.unionButton.UseVisualStyleBackColor = true;
            this.unionButton.Click += new System.EventHandler(this.unionButton_Click);
            // 
            // intersectButton
            // 
            this.intersectButton.Location = new System.Drawing.Point(206, 142);
            this.intersectButton.Name = "intersectButton";
            this.intersectButton.Size = new System.Drawing.Size(79, 36);
            this.intersectButton.TabIndex = 3;
            this.intersectButton.Text = "*";
            this.intersectButton.UseVisualStyleBackColor = true;
            this.intersectButton.Click += new System.EventHandler(this.intersectButton_Click);
            // 
            // SpecialTestForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 196);
            this.Controls.Add(this.intersectButton);
            this.Controls.Add(this.unionButton);
            this.Controls.Add(this.set2TB);
            this.Controls.Add(this.set1TB);
            this.Name = "SpecialTestForm";
            this.Text = "SpecialTestForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox set1TB;
        private System.Windows.Forms.TextBox set2TB;
        private System.Windows.Forms.Button unionButton;
        private System.Windows.Forms.Button intersectButton;
    }
}