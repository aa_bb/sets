﻿namespace CSharpPart1Set
{
    class MultiSet : Set
    {
        int[] m;

        public MultiSet(int count)
        {
            MaxElement = count;
            m = new int[MaxElement];
        }

        public override void Add(int item)
        {
            if (item > MaxElement)
                throw new OutOfBoundsException("Попытка добавить в множество элемент, больший максимального");
            m[item]++;
        }

        public override void Remove(int item)
        {
            m[item] = 0;
        }

        public override bool FindItem(int item)
        {
            return m[item] != 0;
        }
    }
}
